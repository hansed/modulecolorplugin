package org.hansed.moduleColor.notification;

import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.EditorNotificationProvider;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.function.Function;

/**
 * @author HanSed
 */
public class ColorNotification implements EditorNotificationProvider {

    @Override
    public @NotNull Function<? super FileEditor, ? extends JComponent> collectNotificationData(@NotNull Project project, @NotNull VirtualFile virtualFile) {
        return (editor -> {

            String moduleName = "outside module";

            Module module = ProjectFileIndex.getInstance(project).getModuleForFile(virtualFile);

            if (module != null) {
                moduleName = module.getName();
            }
            return new ColoredNotificationPanel(virtualFile.getName() + " " + (moduleName.equals("outside module") ? moduleName : "in module: " + moduleName), moduleName);
        });
    }
}
