package org.hansed.moduleColor.notification;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.ui.EditorNotificationPanel;
import org.hansed.moduleColor.components.ColorStorage;

/**
 * @author HanSed
 * <p>
 * ColoredNotificationPanel, uses the {@link ColorStorage} service to color its background
 */
class ColoredNotificationPanel extends EditorNotificationPanel {

    /**
     * @param text   text to be displayed on the notification
     * @param module name of the module this notification is referencing
     */
    ColoredNotificationPanel(String text, String module) {
        super(ApplicationManager.getApplication().getService(ColorStorage.class).getColor(module));
        setText(text);
        createActionLabel("Change color", () -> ColorChooser.create(module, myBackgroundColor), false);
    }
}
