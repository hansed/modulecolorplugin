# Module Colors

An IntelliJ plugin that adds a colored bar specifying the module that the file you are editing is part of.

![](https://plugins.jetbrains.com/files/18928/screenshot_8bae5d15-b9f8-48d1-9036-de6333c80874)
https://plugins.jetbrains.com/plugin/18928-module-colors
